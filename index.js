var xport=parseInt(process.argv[2]);
if (xport<10000 || xport>10100 || isNaN(xport)) {
	console.log('setting default port 10000');
	xport=10000;
}
// HIHIHIHI :(
var express = require('express');
var app = express();
var http = require('http').Server(app);
var WebSocketServer = require('ws').Server;

// WebSocketServer auf Port expressport+100
var wss = new WebSocketServer({
	port: xport+100	
});

// Verzeichnis für statische HTML-Seiten angeben 
app.use(express.static('public'));
 
// Standard-Seite festlegen
app.get('/', function(req, res) {
	res.sendFile(__dirname + '/index.html');
});

// neue Methode "broadcast" für WebSocketServer erstellen
// Diese Methode sendet eine Nachricht an alle verbundenen Clients.
wss.broadcast = function broadcast(data) {
	wss.clients.forEach(function each(client) {
		client.send(data);
	});
};

// neue Methode "sendPrivate" für WebSocketServer erstellen
// Diese Methode sendet eine Nachricht nur an den Client
// mit dem angegebenen Namen.
wss.sendPrivate = function sendPrivate(data,name) {
	let clients=Array.from(wss.clients);
	console.log(clients);
	for (let i=0;i<clients.length;i++) {
		console.log(clients[i].username);
		if (clients[i].username==name)
			clients[i].send(`<strong> ${name} (priv)</strong>: ${data}`);
	}
	/*wss.clients.forEach(function each(client) {
		if (client.username==name)
			client.send('<strong>' + name + ' (priv)</strong>: ' + data);
	});*/
};
 
// Verhalten bei Verbindungsaufbau
wss.on('connection', function(ws) {

	// und bei Eingang einer Nachricht
	ws.on('message', function(data) {
		data = JSON.parse(data);
		let msg=data.message;
		let nam=data.name;
		if (nam) 
			ws.username=nam;
		if (msg) {
			if (msg.substring(0,2)=='/p') 
				wss.sendPrivate(msg.substring(msg.indexOf(' '),msg.length),nam);
			else 
				wss.broadcast(`<strong> ${nam} </strong>: ${msg}`);
		}
	});
});
 
// Webserver express starten
http.listen(xport, function() {
	console.log('listening on *:'+xport);
});

